#include "stdafx.h"
#include <iostream>
#include <opencv2/ml/ml.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include "windows.h"
#include <fstream>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using namespace cv;

//#define SHOW_IMG        // ���������� ������ ����������� �� ����� �������� � ������ �� ��� �������� �����
//#define SHOW_RESULT_IMG // ���������� ������ ����������� �� ����� ������������� � ������ �� ��� �������� �����

void compute_images(string& path, BOWKMeansTrainer& trainer);
void get_samples(vector<string>& sourcesPath, vector<string>& train, vector<string>& control);
void get_files(const string folder, const string type, vector<string>& out_array);
void print_loader();

// ���������� ������� �������
const int class_count = 2;

const string CLASS1 = "Chair";
const string CLASS2 = "House";

// ����� � ������������� 1 � 2 ������
const string c1_folder1 = "./img/windsor_chair/";
const string c2_folder2 = "./img/houses/";

int main() {
	// �������������� ������ nonfree ��� ������������� ���������� SURF � SIFT
	initModule_nonfree();

	vector<string> sources_path1 = vector<string>();
	vector<string> sources_path2 = vector<string>();

	// �������� ������ ������ 1 ������
	cout << "load 1st class objects" << endl;
	get_files(c1_folder1, "*.jpg", sources_path1);

	// �������� ������ ������ ������� ������
	cout << endl << "load 2nd class objects" << endl;
	get_files(c2_folder2, "*.jpg", sources_path2);

	// ���������� ����������� 1 � 2 ������
	const auto data1_count = sources_path1.size();
	const auto data2_count = sources_path2.size();
	
	// ��������� � ����������� �������
	auto train   = vector<string>();
	auto control = vector<string>();

	// ��������� ��� ����������� �� ��������� � ����������� �������
	get_samples(sources_path1, train, control);
	get_samples(sources_path2, train, control);
	
	const auto class1_count = data1_count / 2; // ���������� �������� 1 ������ � ������ �������
	const auto control_count = control.size(); // ������ ����������� �������

	//������� ������ ������ BOWTrainer � ������  ���������, ������ class_count
	BOWKMeansTrainer bow_trainer(class_count, TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 5, 0.001), 5, KMEANS_PP_CENTERS);
	
	cout << "train begin" << endl << "training...   ";
	
	//������� �������� �� ��������� �������
	for (auto element : train) {
		print_loader();
		compute_images(element, bow_trainer);
	}
	
	cout << "train end" << endl;
	cout << "cluster begin" << endl;
	
	// ���������� �������������
	const auto vocabulary = bow_trainer.cluster();
	
	cout << "cluster end" << endl;

	std::ofstream file("out.txt");

	cout << "Results:" << endl;
	auto error1 = 0;
	auto error2 = 0;

	//�������� �������� �� ����������� �������
	for (auto i = 0; i < control_count; i++) {
		
		const auto src = cv::imread(control[i]);

		auto detector         = FeatureDetector::create("SIFT");
		const auto descriptor = DescriptorExtractor::create("SIFT");
		
		// ������� ������ ������, ���������� ��������� � ����������� ��������
		const auto descriptors_matcher = DescriptorMatcher::create("FlannBased"); 

		Mat descriptors; // ������� ������������
		vector<KeyPoint> keypoints; // ������ �������� �����
		
		// ������� ������ ������, ������������ ����������� �������� �����������
		Ptr<BOWImgDescriptorExtractor> bow_extractor = new BOWImgDescriptorExtractor(descriptor, descriptors_matcher);

		//������������� ������� � ���������� ������
		bow_extractor->setVocabulary(vocabulary);
		detector->detect(src, keypoints);
		bow_extractor->compute(src, keypoints, descriptors);

		// ������� ������ �������������
		const auto c1    = descriptors.at<float>(0, 0);
		const auto c2    = descriptors.at<float>(0, 1);
		const auto is_c1 = c1 > c2;

		file << "0: "  << c1 << " 1: " << c2 << " \t"  << (is_c1? CLASS1 : CLASS2)
			 << " \t"  << ((i < class1_count)? CLASS1 : CLASS2) << endl;

		cout << "img " << i << (is_c1 ? " \t" + CLASS1 : " \t" + CLASS2)
			 << ((i < class1_count) ? " \t" + CLASS1 : " \t" + CLASS2) <<endl;

		if (i < class1_count && !is_c1) {
			error1++;
		} else if (i > class1_count && is_c1) {
			error2++;
		}

		#ifdef SHOW_RESULT_IMG
			Mat out = src.clone();
			drawKeypoints(src, keypoints, out, Scalar::all(-1), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
			imshow("out", out);
			waitKey(0);
		#endif
	}

	const auto error = (error1 + error2) / float(control_count);

	cout << "Recognition error: " << error << endl;
	
	file.close();
	
	cout << "done" << endl;

	int c;
	cin >> c;
	
	return 0;
}

/**
 * \brief ��������� �������� ������� �� ��������� � ����������� ������� ��������� �������.
 * \param sources_path �������� �������
 * \param train [out] ��������� �������
 * \param control [out] ����������� �������
 */
void get_samples(vector<string>& sources_path, vector<string>& train, vector<string>& control) {
	const auto count        = sources_path.size();
	const auto half_count   = count / 2;
	auto ti = 0;
	auto ci = 0;

	for (auto i = 0; i < count; i++) {
		const auto a = rand() % 2;
		
		if (a == 0 && ti < half_count || ci >= half_count) {
			train.push_back(sources_path[i]);
			ti++;
		}
		
		else if (ci < half_count) {
			control.push_back(sources_path[i]);
			ci++;
		}
	}
}

/**
 * \brief ��������� �����������, �������� �������� � ������� ��� BOWKMeansTrainer
 * \param path ��� ������� �������
 * \param trainer ������ �� trainer
 */
void compute_images(string& path, BOWKMeansTrainer& trainer) {
	// ������ ������ �����
	vector<KeyPoint> keypoints;

	// ������� ������ �����
	const auto src = imread(path);
	auto detector = FeatureDetector::create("SIFT"); 
	detector->detect(src, keypoints);

	// ��������� �����������
	Mat descriptors;
	auto descriptor = DescriptorExtractor::create("SIFT");
	descriptor->compute(src, keypoints, descriptors); 

	trainer.add(descriptors);

	#ifdef SHOW_IMG //���������� ������ ����� �� ������� �����������
		Mat out = src.clone();
		drawKeypoints(src, keypoints, out, Scalar::all(-1), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
		imshow("keypoints", out);
		waitKey(0);
	#endif
}

/**
 * \brief ���������� ������ ���� ������ � �������� ����� 
 * \param folder ����� � �������
 * \param type ������ ��� ������. �������� *.jpg
 * \param out_array �������� ������ �� ������� ������
 */
void get_files(const string folder, const string type, vector<string>& out_array) {

	WIN32_FIND_DATA find_file_data;
	auto fullpath = folder + type;

	const HANDLE hf = FindFirstFile(fullpath.c_str(), &find_file_data);

	if (hf != INVALID_HANDLE_VALUE) {
		do {
			const auto str = folder + find_file_data.cFileName;
			out_array.push_back(str);

			cout << str << "\n";
		} while (FindNextFile(hf, &find_file_data) != 0);

		FindClose(hf);
	}
}

/**
 * \brief ������� �������� �������� � �������
 */
void print_loader() {
	static auto counter = 0;
	
	const auto index = counter % 4;
	string s = "";
	
	switch (index) {
	case 0:
		s = "-";
		break;
	case 1:
		s = "\\";
		break;
	case 2:
		s = "|";
		break;
	default:
		s = "/";
		break;
	}
	
	counter++;
	cout << "\b" << s << std::flush;
}